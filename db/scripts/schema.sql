CREATE DATABASE liagame;
USE liagame;

CREATE TABLE teams(
    team_id INT AUTO_INCREMENT PRIMARY KEY,
    name TEXT,
    bot_name TEXT,
    git_url TEXT
);

CREATE TABLE matches(
    match_id INT AUTO_INCREMENT PRIMARY KEY,
    round INT,
    team1 INT,
    team2 INT,
    winner INT,
    duration INT,
    replay_link TEXT
);
