
const express = require('express')
const mysql = require('mysql2/promise');

const app = express()
const port = 3000

app.use('/replays', express.static('/replays'));

app.set('view engine', 'ejs');


const db_connection_pool = mysql.createPool({
  host: 'mysql',
  user: 'root',
  password: "z2D7EB7cRQY5Wf4k",
  database: 'liagame',
  waitForConnections: true,
  connectionLimit: 5,
  queueLimit: 0
});

/*
db_connection = await mysql.createConnection({
  host: 'mysql', //"localhost",
  user: "root",
  password: "z2D7EB7cRQY5Wf4k",
  database: "liagame"
});
*/

async function query(sql, params = []) {

  const [results,] = await db_connection_pool.execute(sql, params);

  return results;
}


app.get('/', async (req, res) => {

  const teams = await query('SELECT * FROM teams');
  const matches = await query('SELECT * FROM matches');


  const team_dict = {}
  for (t of teams) {
    t.points = 0
    team_dict[t.team_id] = t
  }

  //console.log(team_dict)

  for (m of matches) {
    team_dict[m.winner].points++

    m.team1 = team_dict[m.team1].name
    m.team2 = team_dict[m.team2].name
    m.winner = team_dict[m.winner].name
  }

  //console.log(matches);

  matches.sort((a, b) => b.round - a.round)
  teams.sort((a, b) => b.points - a.points)


  res.render('home.ejs', { teams, matches });
})

app.listen(port, '0.0.0.0', () => {
  console.log(`Example app listening at 0.0.0.0:${port}`)
})
