import subprocess
from sqlalchemy import create_engine, select
from sqlalchemy.orm import sessionmaker, scoped_session
import sys
import os
import itertools
from time import time, sleep

roundLength=5*60

class Backend():
    def __init__(self):
        self.engine = create_engine("mysql+pymysql://root:z2D7EB7cRQY5Wf4k@mysql/liagame", pool_size=1024, max_overflow=0, isolation_level="READ COMMITTED")
        self.conn = self.engine.connect()
        self.session = sessionmaker(bind=self.engine)
        self.scoped_session = scoped_session(self.session)

    def query(self, query):
        return self.conn.execute(query).fetchall()

    def getTeams(self):
        return self.conn.execute("SELECT * FROM teams").fetchall()

    def insertGameResult(self, gameData):
        self.conn.execute(f"INSERT INTO matches(round, team1, team2, winner, duration, replay_link) VALUES ({gameData['round']}, {gameData['team1']}, {gameData['team2']}, {gameData['winner']}, {gameData['duration']}, \"{gameData['replay_link']}\")")

    def get_latest_round(self):
        if self.conn.execute("SELECT COUNT(*) AS C FROM matches").fetchall()[0]["C"]==0:
            return 0
        return self.conn.execute("SELECT MAX(round) AS C FROM matches").fetchall()[0]["C"]+1

backend=None
while(backend is None):
    try:
        backend=Backend()
    except Exception:
        print("Waiting for db to be ready....")
        sleep(10)

def runGame(bot1, bot2, replay_link):
    output=subprocess.run(["./lia", "generate", bot1, bot2, "-r", replay_link], check=True, stdout=subprocess.PIPE, timeout=30, stderr=subprocess.STDOUT).stdout.decode()
    #print(output)
    if "-------------- Game finished --------------" in output:
        duration=int(float(output.split("Game duration: ")[1].split("\n")[0]))
        winner=output.split("Winner: ")[1].split("\n")[0]
        if winner=="Bot 2":
            winner=bot2
        else:
            winner=bot1
        return winner, duration
    return None, None


def runRound(roundId):
    teams=backend.getTeams()
    teams_data={}
    for team in teams:
        teams_data[team["bot_name"]]={"teamName": team["name"], "teamId": team["team_id"], "botName": team["bot_name"], "gitUrl": team["git_url"]}
    for team in teams_data.keys():
        bot_name=team
        os.system(f"cd {bot_name}; git pull")
        os.system(f"./lia compile {bot_name}")
    matches=list(itertools.combinations(list(teams_data.keys()), 2))
    print(matches)
    for match in matches:
        replay_link=f"/replays/round_{roundId}_match_{teams_data[match[0]]['teamName']}_{teams_data[match[1]]['teamName']}.lia"
        result, duration=runGame(match[0], match[1], replay_link)
        if result is None or duration is None:
            print(f"Wtf is going on here? Match between {match[0]} and {match[1]} failed!")
        else:
            game_result={}
            game_result['round']=roundId
            game_result['team1']=teams_data[match[0]]["teamId"]
            game_result['team2']=teams_data[match[1]]["teamId"]
            game_result['winner']=teams_data[result]["teamId"]
            game_result['duration']=duration
            game_result['replay_link']=replay_link
            backend.insertGameResult(game_result)

def main_loop(current_round):
    while(True):
        print(f"Running round {current_round}")
        startTime=time()
        runRound(current_round)
        endTime=startTime+roundLength
        while(time()<endTime):
            sleep(10)
        current_round+=1


def cold_start():
    current_round=backend.get_latest_round()
    main_loop(current_round)
