#!/usr/bin/python3 -u

import json
import os
from sqlalchemy import create_engine, select
from sqlalchemy.orm import sessionmaker, scoped_session
import sys
import time
from gamemanager import cold_start
import shutil


class Backend():
    def __init__(self):
        self.engine = create_engine("mysql+pymysql://root:z2D7EB7cRQY5Wf4k@mysql/liagame", pool_size=1024, max_overflow=0, isolation_level="READ COMMITTED")
        self.conn = self.engine.connect()
        self.session = sessionmaker(bind=self.engine)
        self.scoped_session = scoped_session(self.session)

    def query(self, query):
        return self.conn.execute(query)

    def insertTeam(self, team):
        self.conn.execute(f"INSERT INTO teams(name, bot_name, git_url) VALUES (\"{team['name']}\", \"{team['bot_name']}\", \"{team['git_url']}\")")

print("Booting up...")
backend=None
while(backend is None):
    try:
        backend=Backend()
    except Exception:
        print("Waiting for db to be ready....")
        time.sleep(10)

if os.path.isfile("./config.json"):
    print("Config found, blank start")
    os.system("echo 'n' | ./lia")
    shutil.rmtree('/replays/', ignore_errors=True)
    backend.query("DELETE FROM matches;")
    backend.query("DELETE FROM teams;")
    config=json.load(open("./config.json", "r"))
    teams=config['teams']
    for team in teams:
        backend.insertTeam(team)
        os.system(f"git clone {team['git_url']} {team['bot_name']}")
    os.remove("./config.json")

cold_start()
